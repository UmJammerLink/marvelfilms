import { Component, OnInit } from '@angular/core';

import { Film } from '../../interfaces/film.interface';

import { FilmService } from '../../services/film.service';

@Component({
  selector: 'app-list-films',
  templateUrl: './list-films.component.html',
  styleUrls: ['./list-films.component.scss']
})
export class ListFilmsComponent implements OnInit {

  public filmArray: Film[] = [];

  constructor(private filmService: FilmService) { }

  ngOnInit(): void {
    this.getFilms();
  }

  getFilms(): void{
    this.filmService.getListFilms()
    .subscribe((response) => {
      this.filmArray = response;
      console.log(this.filmArray);
    });
  }

}
