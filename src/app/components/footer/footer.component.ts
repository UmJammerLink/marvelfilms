import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public title = 'Marvel Films';
  public copyright = 'Curso Angular DEW II @2021';

  constructor() { }

  ngOnInit(): void {
  }

}
