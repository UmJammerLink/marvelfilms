import { Component, OnInit } from '@angular/core';

// Nos permite recoger la route del componente y poder parametrizar sus variables
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { Film } from '../../interfaces/film.interface';

import { FilmService } from '../../services/film.service';
import { Subscriber } from 'rxjs';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: [ './detail.component.scss' ]
})
export class DetailComponent implements OnInit {
  
  public film: Film = {};

  constructor(
    private route: ActivatedRoute,
    private filmService: FilmService
  ) {}

  ngOnInit(): void {
    this.getDetailFilm();
  }

  getDetailFilm(): void {
    // Parseo de la route y recogemos el id
    // (definido en el app-routing.module.ts, en el path que pinta el componente)
    const id = this.route.snapshot.paramMap.get('id');
    if(id){
      this.filmService.getDetailFilm(id)
      .subscribe((result) => {
        if(this.isEmptyObject(result)){
          // Redirijo a un 404
          console.log('404');
        }
        else{
          this.film = result;
          console.log(this.film);
        }
      });
    }
    
  }

  isEmptyObject(obj: Film) {
    return (obj && (Object.keys(obj).length === 0));
  }

}

