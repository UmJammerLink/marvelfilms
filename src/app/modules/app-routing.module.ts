import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { ListFilmsComponent } from '../components/list-films/list-films.component';
import { DetailComponent } from '../components/detail/detail.component';

const routes: Routes = [
  { path: '', component:  ListFilmsComponent },
  { path: 'detail/:id', component: DetailComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
