export interface Film{
    id?: number;
    name?: string;
    poster?: string;
    releaseDate?: Date;
    description?: string;
}