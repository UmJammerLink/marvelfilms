import { Injectable } from '@angular/core';

// Para poder realizar peticiones HTTP y usar observables
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';

import { environment } from '../../environments/environment';

import { Film } from '../interfaces/film.interface';

@Injectable({ providedIn: 'root' })

export class FilmService {

  private listFilmsUrl =  environment.apiUrl + '/films.php';
  private detailFilmUrl = environment.apiUrl + '/films.php?id=';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin':'*'
    })
  };

  constructor(private http: HttpClient) { }

  getListFilms(): Observable<Film[]>{
    return this.http.get<Film[]>(this.listFilmsUrl, { headers: this.httpOptions.headers });
  }

  getDetailFilm(id: string): Observable<Film>{
    return this.http.get<Film>(this.detailFilmUrl + id, { headers: this.httpOptions.headers });
  }

}
