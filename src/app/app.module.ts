import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

// Necesario para utilizar providers a lo largo de la aplicación
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './modules/app-routing.module';
import { AppComponent } from './components/app.component';

import { DetailComponent } from './components/detail/detail.component';
import { ListFilmsComponent } from './components/list-films/list-films.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { FooterComponent } from './components/footer/footer.component';

import { FilmService } from './services/film.service';

@NgModule({
  declarations: [
    AppComponent,
    DetailComponent,
    ListFilmsComponent,
    TopbarComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    FilmService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
